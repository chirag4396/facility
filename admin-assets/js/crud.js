$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});
var handleDataTableButtons = function(id) {
	var i = '#'+id+'Table';
	if ($(i).length) {
		$(i).DataTable().destroy();
		$(i).DataTable({
			dom: "Bfrtip",
			buttons: [
			{
				extend: "copy",
				className: "btn-sm"
			},
			{
				extend: "csv",
				className: "btn-sm"
			},
			{
				extend: "excel",
				className: "btn-sm"
			},
			{
				extend: "pdfHtml5",
				className: "btn-sm"
			},
			{
				extend: "print",
				className: "btn-sm"
			},
			],
			responsive: true
		});
	}
};

Table = function() {    
	return {
		init: function(id) {
			handleDataTableButtons(id);
		}
	};
}();

(function( $ ) {	
	$.fn.CRUD = function(options, t = 'add' ) {

		var self = this;

		var settings = $.extend( {}, {
			url : '',
			type : 0,
			method : ['post', 'get', 'update', 'delete'],
			finalUrl : options.url,
			validation : true			
		}, options );

		function sendFormData(){
			var formData = new FormData($(self)[0]);

			if(settings.extraVariables){
				$.each(settings.extraVariables(),function(k,v){

					if ($.isArray(v)) {	
						$.each(v,function(d,e) {
							formData.append(k+'[]',e);
						});			
					}else{
						formData.append(k,v);

					}
				});
			}

			switch(settings.type){
				case 0:
				var msgType = (settings.onLoad) ? settings.onLoad : 'add';				
				break;
				case 2:
				settings.finalUrl = settings.url+'/'+formData.get('id');
				formData.append('_method','patch');
				var msgType = (settings.onLoad) ? settings.onLoad : 'update';				
				break;
			}

			self.MSG({type:msgType});
			$.ajax({
				url : settings.finalUrl,
				data : formData,
				type : settings.method[0],
				processData : false,
				contentType : false,
				success : function(data){
					(options.processResponse) ? options.processResponse(data) : '';
					self.MSG({type:data.msg});
				},error : function(xhr,data){
					(settings.processError) ? settings.processError(data) : self.MSG({type:data.msg});
				}
			});
		};

		function formSubmission() {			
			$(self).on({
				'submit' : function(e){
					// console.log(self);
					e.preventDefault();
					var check = [false];
					$.each($(self)[0],function(k,v){
						var attribute = $(v).attr('data-validate')
						if(attribute){
							var validate = attribute.split('|');
							check.push($.inArray(true,self.VALIDATE({type:validate,name:$(v)})) >= 0);
						}
					});
					if($.inArray(true, check) < 0){						
						// if(!settings.validation){
							if(settings.noAjax){
								$(this)[0].submit();
							}else{
								sendFormData();
							}
						// }
					}        		
				}
			});  
		}
		
		function fetchData() {
			// console.log(self);
			// var formData = new FormData();

			$.ajax({
				url : settings.url,
				// data : formData,
				type : settings.method[1],
				success : function(data){					
					(options.processResponse) ? options.processResponse(data) : '';
					// self.MSG({type:'success'});
				},error : function(xhr,data){
					(settings.processError) ? settings.processError(data) : self.MSG({type:'error'});
				}
			});
		}
		
		this.each(function() {
			switch (t){
				case 'add':
				formSubmission();
				break;
				case 'get':
				fetchData();
				break;
			}		
		});
		return this;
	};
	
	// $.fn.CRUD.fetchData = function( options ){
	// 	$.fn.CRUD.fetchData(options);
	// };
	
	$.fn.MSG = function( options ){		
		var settings = $.extend( {}, {
			id : '#formAlert',
			msgDetail :function(fetch){				
				var msgs = {
					add : {
						msg : 'Adding, Please wait!!', 
						css : 'warning'
					},
					check : {
						msg : 'Checking, Please wait!!', 
						css : 'warning'
					},
					update : {
						msg : 'Updating, Please wait!!', 
						css : 'warning'
					},
					success : {
						msg : 'Successfully Added!!', 
						css : 'success'
					},
					successEnquiry : {
						msg : 'We got you Query, we\'ll get back to you soon!!', 
						css : 'success'
					},
					successU : {
						msg : 'Successfully Updated!!', 
						css : 'success'
					},
					successLogin : {
						msg : 'Successfully Logged in!!', 
						css : 'success'
					},
					successRegister : {
						msg : 'Successfully Registered!!', 
						css : 'success'
					},
					successSend : {
						msg : 'Quote Successfully sent!!', 
						css : 'success'
					},
					error : {
						msg : 'Something went wrong, Please try again!!', 
						css : 'danger'
					},
					errorLogin : {
						msg : 'Invalid credentials, Please try again!!', 
						css : 'danger'
					},
					errorD : {
						msg : 'Something went wrong, try again or these data must be linked with other child data!!', 
						css : 'danger'
					},
					exist : {
						msg : 'Record Exist, Please try something new!!', 
						css : 'warning'		
					},
					same : {
						msg : 'Same as Previous, Please try something new!!', 
						css : 'warning'
					},
					delete : {
						msg : 'Record Deleted Successfully!!', 
						css : 'success'		
					}
				}
				return msgs[fetch];
			},
			input : function(fetch){
				var msgs = {
					select : 'Please select option',
					empty : 'These field couldn\'t be Empty',
					mobile : 'Invalid Mobile Number',
					email : 'Invalid Email address',
					alphaSpace : 'Name must contain characters and space',
					minimum : 'Should be a length of 8',
					oneDigit : 'Should contain 1 digit',
					oneLower : 'Should contain 1 Lowercase letter',
					oneUpper : 'Should contain 1 Uppercase letter',
					oneSpecial : 'Must contain 1 Special character like @,$,#,etc...',
					same : 'Password d oesn\'t match',
					digits : 'Please enter digits',
					same : 'Password d oesn\'t match',
					date : 'Invalid Date'
				}
				return msgs[fetch];
			},
			type:'add',
			keep: false,
			milisecond: 3000,
			for : 'form'
		}, options );

		timer = function(process){    		
			window.setTimeout(function(){
				process();
			},settings.milisecond);
		};

		removeInputText = function(name) {    		
			$('#'+name).remove();
		};

		inputText = function(name){
			var span = $('<p/>',{
				id : settings.id,
				style : 'color: #db0c06;',
				html : settings.input(settings.validationType)
			});		
			removeInputText(settings.id);
			$(name).focus().after(span);
		};	

		formText = function(name){
			// console.log(name);
			$('<div/>',{
				id : 'formAlert',
				class : 'text-center alert alert-'+settings.msgDetail(settings.type).css+' fade in',
				html : settings.msgDetail(settings.type).msg
			}).appendTo(name);
			var successText = ['success', 'successU'];
			// if (!settings.keep) {
				var formId = $(settings.id).parent().attr('id').replace('Form','');
				timer(function(){
					if ($.inArray(settings.type, successText) > -1) {
						$(name)[0].reset();
						if($('#'+formId+'Modal')){
							$('#'+formId+'Modal').modal('toggle');;
						}
						$(settings.id).remove();
					}
				});	   		
			// }
		};

		return this.each(function(){  

			if($(settings.id).length){
				$(settings.id).remove();
			}

			switch(settings.for){
				case 'form' :
				formText(this);
				break;

				case 'input' :
				inputText(this);
				break;				
				
				case 'remove' :
				removeInputText(settings.id);
				break;
			}			
		});
	};

	$.fn.VALIDATE = function (options) {
		var settings = $.extend( {}, {    		
			alphaSpace : /^[a-zA-Z\s]+$/,
			empty : /^$/,
			email : /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
			mobile : /^\d{10}$/,
			digits : /^\d$/,
			select : /^-1$/,
			date : /^([0-9][1-2])\/([0-2][0-9]|[3][0-1])\/((19|20)[0-9]{2})$/
		}, options );

		function processValidate(name,v) {
			var pattern = (( v == 'empty') ? (settings[v].test($(name).val())) : (!settings[v].test($(name).val())));
			
			if( v == 'select' ){
				pattern = (settings[v].test($(name).val()));
			}
			
			if( v == 'digits' ){
				pattern = (settings[v].test($(name).val()));
			}

			if (pattern){
				$(name).MSG({id:$(name).attr('name')+v,validationType:v,for : 'input'});
				return true;
			}
			$(name).MSG({id:$(name).attr('name')+v,validationType:v,for : 'remove'});
			return false;			
		}

		var chk = [];

		$.each(settings.type,function(k,v){    		
			chk.push(processValidate(settings.name,v));    			
		});

		return chk;    	
	}

}( jQuery ));

var height,width;
// $(function() {
	function imageUpload(id) {
		
		$('#'+id).on("change", function(){
			var files = !!this.files ? this.files : [];

			if (!files.length || !window.FileReader) return;

			if (/^image/.test( files[0].type)){
				var reader = new FileReader();
				reader.readAsDataURL(files[0]);            
				reader.onloadend = function(upImg){

					$("#"+id+"Preview").attr("src", this.result);
					urlValue = this.result;

					var image = new Image();
					image.src = upImg.target.result;

					image.onload = function() {
						height = this.height;
						width = this.width;					
					};
				}                
			}
		});
	}

// });
// if(data.length){
// var tr = [];
// var th = [];
// var ke = Object.keys(data[0]);
//
// var seq = ['title','hours',];
// $.each(data,function(k,v){
// 	var keys = Object.keys(v);
// 	var objectLength = keys.length;
// 	var td = [];

// 	for(var i = 0; i < objectLength; i++){
// 		td.push($('<td/>', {
// 			html : v[keys[i]]
// 		}));
// 	}
// 	tr.push($('<tr/>', {
// 		html : td
// 	}));
// });

// }
function multiSelect(id, link) {
	var self = id;
	$(id).select2({
		tags:true,
		placeholder: 'Start typing slowly',
		allowClear: true,
		createTag: function (params) {
			var term = $.trim(params.term);

			if (term === '') {
				return null;
			}

			return {
				id: term,
				text: term,
				newTag: true
			}
		}
	});
	$(id).on('select2:select', function (e) {
		var data = e.params.data;
		var old = data.id;
		console.log(old);
		if(data.newTag){
			$.post(geturl(link), {title: data.text}, function(data) {
				// console.log(data);
				$(self).find("option[value='" + old + "']").remove();
				var newOption = new Option(data.d.title, data.d.id, true, true);
				$(self).append(newOption).trigger('change');				
			});
		}
	});
}