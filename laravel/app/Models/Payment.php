<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	public $primaryKey = 'ph_id';

	protected $fillable = ['ph_order_id', 'ph_status', 'ph_txn_id', 'ph_amount', 'ph_fullname','ph_email', 'ph_phone', 'ph_bank_ref_num', 'ph_bankcode', 'ph_unmapped', 'ph_added_on', 'ph_created_at' ];

	CONST CREATED_AT = 'ph_created_at';

	CONST UPDATED_AT = null;

	public function order()
	{
		return $this->belongsTo(\App\Models\OrderDetail::class, 'ph_order_id');
	}
}
