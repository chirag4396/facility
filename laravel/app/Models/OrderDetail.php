<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	protected $primaryKey = 'order_id';

	protected $fillable = ['order_d_id', 'order_company', 'order_name', 'order_email', 'order_mobile'];

	CONST CREATED_AT = 'order_created_at';
	
	CONST UPDATED_AT = null;

	public function payment()
	{
		return $this->hasOne(\App\Models\Payment::class, 'ph_order_id');
	}
	
	public function company()
	{
		return $this->belongsTo(\App\Models\Company::class, 'order_company');
	}
}
