<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
	protected $primaryKey = 'enq_id';

	protected $fillable = ['enq_full_name', 'enq_category','enq_email', 'enq_company', 'enq_mobile', 'enq_query', 'enq_user', 'enq_from', 'enq_to'];

	CONST CREATED_AT = 'enq_created_at';
	
	CONST UPDATED_AT = 'enq_updated_at';  

	public function company(){
	  	return $this->belongsTo(\App\Models\Company::class, 'enq_company');
	}  

	public function category(){
	  	return $this->belongsTo(\App\Models\SubCategory::class, 'enq_category');
	}
}
