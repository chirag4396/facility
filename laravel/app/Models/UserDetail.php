<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{

	protected $primaryKey = 'ud_id';

	protected $fillable = ['ud_user', 'ud_emp_id', 'ud_address', 'ud_gender', 'ud_category', 'ud_qualification', 'ud_language', 'ud_img_path', 'ud_dob', 'ud_mobile', 'ud_alt_mobile', 'ud_identity_proof', 'ud_address_proof', 'ud_identity_no', 'ud_address_no','ud_address_proof', 'ud_permanent_address', 'ud_current_address', 'ud_mother_tongue', 'ud_blood_group', 'ud_height_feet', 'ud_height_inches'];

	CONST CREATED_AT = 'ud_created_at';
	
	CONST UPDATED_AT = 'ud_updated_at';

    public function user(){
        return $this->belongsTo(\App\User::class, 'ud_user');
    }
}
