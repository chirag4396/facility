<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $primaryKey = 'con_id';

    protected $fillable = ['con_name', 'con_mobile', 'con_email', 'con_subject', 'con_query'];

    CONST CREATED_AT = 'con_created_at';
    
    CONST UPDATED_AT = null;  
}
