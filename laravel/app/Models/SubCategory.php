<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
	protected $primaryKey = 'scat_id';

	protected $fillable = ['scat_title', 'scat_cat_id', 'scat_img_path', 'scat_img_thumb_path'];

	public $timestamps = false;

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class, 'scat_cat_id');
	}

	public function enquiry(){
	  	return $this->hasMany(\App\Models\Enquiry::class, 'enq_category');
	}
}
