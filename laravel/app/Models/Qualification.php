<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $primaryKey = 'qua_id';

    protected $fillable = ['qua_title'];

	public $timestamps = false;
}
