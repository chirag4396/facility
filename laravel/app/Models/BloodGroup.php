<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BloodGroup extends Model
{
	protected $primaryKey = 'bg_id';

	protected $fillable = ['bg_title'];

	public $timestamps = false;
}
