<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $primaryKey = 'cmp_id';

    protected $fillable = ['cmp_full_name', 'cmp_title', 'cmp_mobile', 'cmp_email', 'cmp_city'];

    CONST CREATED_AT = 'cmp_created_at';
    
    CONST UPDATED_AT = 'cmp_updated_at';

    public function enquiries()
    {
      	return $this->hasMany(\App\Models\Enquiry::class, 'enq_company');
    }  

    public function order()
    {
    	return $this->hasMany(\App\Models\OrderDetail::class, 'order_company');
    }
}
