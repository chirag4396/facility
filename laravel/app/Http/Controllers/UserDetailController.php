<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class UserDetailController extends Controller
{
    use GetData;

    protected $response = ['msg' => 'error'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_worker');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function images(Request $r)
    {        
        // return count($r->user_img);
        // foreach ($r->user_img as $k => $value) {         
            list($identity) = $this->uploadFiles($r, 'svm'.rand(9999,2), 'user_img', ['images/others/'], [], 1600);
        // }
    }
    public function store(Request $r)
    {
        $identity = 'images/no-image.png';
        $address = 'images/no-image.png';
        $user_img = 'images/blank-user.jpg';
        if (User::where('email', $r->email)->first()) {
            $this->response['msg'] = 'exist';            
            return $this->response;
        }
        if ($r->hasFile('id_card')) {            
            list($identity) = $this->uploadFiles($r, $r->name, 'id_card', ['images/identities/']);
        }
        if ($r->hasFile('add_card')) {            
            list($address) = $this->uploadFiles($r, $r->name, 'add_card', ['images/address/']);
        }
        if ($r->hasFile('user_img')) {            
            list($user_img) = $this->uploadFiles($r, $r->name, 'user_img', ['images/users/']);
        }
        $ud = $this->changeKeys('ud_',$r->all());
        $u = [
            'name' => $ud['ud_name'],
            'email' => $ud['ud_email'],
            'password' => bcrypt($this->randomPassword(8))
        ];

        $user = User::create($u);

        $ud['ud_emp_id'] = $ud['ud_emp_id'];
        $ud['ud_user'] = $user->id;
        $ud['ud_identity_proof'] = $identity;
        $ud['ud_address_proof'] = $address;
        $ud['ud_img_path'] = $user_img;
        $ud['ud_category'] = implode(',', $ud['ud_cat']);
        $ud['ud_language'] = implode(',', $ud['ud_lang']);
        $ud['ud_qualification'] = implode(',', $ud['ud_qualifications']);
        
        unset($ud['ud_name']);
        unset($ud['ud_email']);
        unset($ud['ud_id_card']);
        unset($ud['ud_lang']);
        unset($ud['ud_qualifications']);
        unset($ud['ud_cat']);
        unset($ud['ud_add_card']);
        unset($ud['ud_user_img']);
        
        try {
            $udetail = UserDetail::create($ud);            
            if($udetail){
                $this->response['msg'] = 'success';
            }
        } catch (QueryException $e) {
            User::find($user->id)->delete();            
            return $this->response;
        }            

        return $this->response;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function show(UserDetail $userDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(UserDetail $userDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserDetail $userDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDetail $userDetail)
    {
        //
    }
}
