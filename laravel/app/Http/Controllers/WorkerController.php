<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use App\Models\UserDetail;
use App\User;
use App\Models\SubCategory;

class WorkerController extends Controller
{
    use GetData;

    protected $response = ['msg' => 'error'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search($category = null, $from = null, $to = null)
    {        
        $users = UserDetail::whereOr(function($users) use ($category){
            $cats = explode(',', $category);
            foreach ($cats as $k => $cat) {            
                $users->whereRaw('FIND_IN_SET('.$cat.',ud_category)');
            }
        })->get();
        return view('user.list')->with(['users' => $users, 'category' => $category, 'from' => $from, 'to' => $to]);
    }

    public function index()
    {
        $users = User::where('type', 103)->get();

        return view('admin.view_employees')->with(['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_worker');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return User::find($id);
        return view('admin.edit_employee')->with(['emp' => User::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        // return $r->all();
        $identity = 'images/no-image.png';
        $address = 'images/no-image.png';
        $user_img = 'images/blank-user.jpg';

        $us = User::find($id);
        // if (User::where('email', $r->email)->first()) {
        //     $this->response['msg'] = 'exist';            
        //     return $this->response;
        // }
        if ($r->hasFile('id_card')) {            
            list($identity) = $this->uploadFiles($r, $r->name, 'id_card', ['images/identities/']);
        }
        if ($r->hasFile('add_card')) {            
            list($address) = $this->uploadFiles($r, $r->name, 'add_card', ['images/address/']);
        }
        if ($r->hasFile('user_img')) {            
            list($user_img) = $this->uploadFiles($r, $r->name, 'user_img', ['images/users/']);
        }
        $ud = $this->changeKeys('ud_',$r->all());
        $u = [
            'name' => $ud['ud_name'],
            'email' => $ud['ud_email'],
            'password' => bcrypt($this->randomPassword(8))
        ];

        $us->update($u);
        
        $user = $us;

        $ud['ud_emp_id'] = $ud['ud_emp_id'];
        $ud['ud_user'] = $user->id;
        $ud['ud_identity_proof'] = $identity;
        $ud['ud_address_proof'] = $address;
        $ud['ud_img_path'] = $user_img;
        $ud['ud_category'] = implode(',', $ud['ud_cat']);
        $ud['ud_language'] = implode(',', $ud['ud_lang']);
        $ud['ud_qualification'] = implode(',', $ud['ud_qualifications']);
        
        unset($ud['ud_name']);
        unset($ud['ud_id']);
        unset($ud['ud_email']);
        unset($ud['ud_id_card']);
        unset($ud['ud_lang']);
        unset($ud['ud_qualifications']);
        unset($ud['ud_cat']);
        unset($ud['ud_add_card']);
        unset($ud['ud_user_img']);
        unset($ud['ud__method']);
        
        $udetail = UserDetail::where('ud_user', $user->id)->update($ud);            
        try {
            if($udetail){
                $this->response['msg'] = 'successU';
            }
        } catch (QueryException $e) {
            User::find($user->id)->delete();            
        }            

        return $this->response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldUser = User::find($id);
        $oldDetail = $oldUser->detail;
        
        if($oldDetail){            
            $this->removeFile($oldDetail->ud_img_path);
            $this->removeFile($oldDetail->ud_address_proof);
            $this->removeFile($oldDetail->ud_identity_proof);
            $oldDetail->delete();
        }

        $oldUser->delete();

        return redirect()->back();
    }
}
