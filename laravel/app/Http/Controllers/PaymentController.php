<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    use GetData;
    //M3PMW8w0
    protected $MERCHANT_KEY = "M3PMW8w0";
    protected $SALT = "peAOL2ByVK";
    protected $PAYU_BASE_URL = "https://secure.payu.in";
    protected $action = '';
    protected $hash = '';
    protected $txnid = '';
    protected $formError = 0;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::guest()) {
            return redirect()->route('payment.create');            
        }
        return view('admin.view_payments')->with(['payments' => Payment::get()]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $r)
    {
        list($posted, $this->txnid) = $this->listd($r);

        return view('user.payment')->with(['hash' => $this->hash, 'formError' => $this->formError, 'action' => url('placeorder'), 'MERCHANT_KEY' => $this->MERCHANT_KEY, 'txnid' => $this->txnid, 'data' => $r->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }

    public function getPay(Request $r)
    {
        // return $r->all();
        $posted = array();
        if (!empty($r)) {

            foreach ($r->all() as $key => $value) {
                $posted[$key] = $value;

            }
        }


        // return $posted;
        if (empty($posted['txnid'])) {
            $this->txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            
            // if ($userDetail) {                
                $orderDetail = OrderDetail::create([
                    'order_d_id' => 'DFMSPL' . rand(99999, 4).(OrderDetail::max('order_id')+1),
                    'order_total' => $r->amount,
                    'order_company' => $r->company,
                    'order_name' => $r->firstname,
                    'order_email' => $r->email,
                    'order_mobile' => $r->phone,
                    'order_city' => $r->city,
                    'order_address' => $r->address,                  
                    'order_pincode' => $r->pincode
                ]);                
            // }

            // // $posted['productinfo'] = $r->all;
            // // return $posted;
            // $r->session()->put('productinfo', $r->all);
            $r->session()->put('order_id', $orderDetail->order_id);
            // $r->session()->put('user_detail', $userDetail->ud_id);
            $this->txnid = $posted['txnid'];
        }
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if (empty($posted['hash']) && sizeof($posted) > 0) {
            if (
                empty($posted['key'])
                || empty($posted['txnid'])
                || empty($posted['amount'])
                || empty($posted['firstname'])
                || empty($posted['email'])
                || empty($posted['phone'])
                || empty($posted['productinfo'])
                || empty($posted['surl'])
                || empty($posted['furl'])
                || empty($posted['service_provider'])
            ) {
                $this->formError = 1;
                // return $posted;
            } else {
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach ($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                    $hash_string .= '|';
                }

                $hash_string .= $this->SALT;


                $this->hash = strtolower(hash('sha512', $hash_string));
                $this->action = $this->PAYU_BASE_URL . '/_payment';
            }
        } elseif (!empty($posted['hash'])) {
            $this->hash = $posted['hash'];
            $this->action = $this->PAYU_BASE_URL . '/_payment';
        }

        return view('user.payment')->with([
            'hash' => $this->hash,
            'formError' => $this->formError,
            'action' => $this->action,
            'MERCHANT_KEY' => $this->MERCHANT_KEY,
            'txnid' => $this->txnid,
            'posted' => $posted
        ]);
    }
    
    public function orderStatus(Request $r)
    {
        // return $r->all();
        $status = $r['status'];
        $firstname = $r["firstname"];
        $amount = $r["amount"];
        $txnid = $r["txnid"];
        $posted_hash = $r["hash"];
        $key = $r["key"];
        $productinfo = $r["productinfo"];
        $email = $r["email"];
        $salt = $this->SALT;

        if (isset($r["additionalCharges"])) {
            $additionalCharges = $r["additionalCharges"];
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

        } else {

            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

        }
        $hash = hash("sha512", $retHashSeq);

        if ($hash != $posted_hash) {
            return "Invalid Transaction. Please try again";
        } else {
            $status = false;
            // parse_str($r->session()->get('productinfo'),$otherData);
            // $ud = UserDetail::find($r->session()->get('user_detail'));
            // // return $r->all();
            $orderId = $r->session()->get('order_id');
            $orderDetail = OrderDetail::where('order_id', $orderId)->first();
            // $orderDetail->update([
            //     'order_status' => ($r->status == 'success' ? 2 : 3)
            // ]);

            $ph = Payment::create([
                'ph_order_id' => $orderId,
                'ph_email' => is_null($r->email) ? $orderDetail->order_email : $r->email,
                'ph_phone' => is_null($r->phone) ? $orderDetail->order_mobile : $r->phone,
                'ph_status' => $r->status,
                'ph_txn_id' => $r->txnid,
                'ph_amount' => $r->amount,
                'ph_fullname' => $r->firstname,
                'ph_bank_ref_num' => $r->bank_ref_num,
                'ph_bankcode' => $r->bankcode,
                'ph_unmapped' => $r->unmappedstatus,
                'ph_added_on' => $r->addedon
            ]);

            parse_str($r->session()->get('productinfo'), $otherData);
            
            if ($ph) {
                                
                $msg = 'We recieved Rs. '.number_format($ph->ph_amount,2).' on behalf of order '.$orderDetail->order_d_id;

                if ($r->status == 'success') {
                    $this->sendSMS($ph->ph_phone, $msg);
                //     $this->sendEmail($r->email, $r->firstname, ['id' => $orderId, 'msg' => $msg], 'notification');
                    
                    $status = true;
                }

            }
            // return $otherData;
            return redirect()->route('previous',['id' => $orderDetail->order_id, 'status' => $status]);

            // return view('user-panel.previous_checkout')->with(['data' => $otherData, 'payment' => $r->all()]);
        }
    }

    public function listd(Request $r)
    {

        $posted = array();
        if (!empty($r)) {
            foreach ($r->all() as $key => $value) {
                $posted[$key] = $value;

            }
        }

        if (empty($posted['txnid'])) {
            $this->txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $this->txnid = $posted['txnid'];
        }

        return [$posted, $this->txnid];
    }

    public function previous($id, $status = false, Request $r){        
        $order = OrderDetail::find($id);
        return view('user.payment_status')->with(['order' => $order, 'status' => $status]);
    }
}
