<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return view('user.index');
    }

    public function search($id, $name = null)
    {
        return view('user.categories')->with(['catId' => $id]);
    }    

    public function job_register()
    {
        return view('user.register');
    }
    
    public function about()
    {
        return view('user.about');
    }    
}
