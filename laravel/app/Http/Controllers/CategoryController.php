<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;

class CategoryController extends Controller
{
    use GetData;

    protected $response = ['msg' => 'error'];

    protected $path = 'images/categories/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_categories')->with(['categories' => Category::get()]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_category');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        // if(!Category::where('cat_name',$r->name)->first()){
        //     $q = $this->changeKeys('cat_', $r->all());
        //     $q['cat_name'] = ucfirst($q['cat_name']);            

        //     if ($r->hasFile('banner')) {
        //         list($regular, $thumb) = $this->uploadFiles($r, $q['cat_name'], 'banner', [$this->path, $this->pathThumb]);
        //         $q['cat_icon_path'] = $regular;
        //         $q['cat_icon_thumb_path'] = $thumb;
        //     }

        //     $data['msg'] = Category::create($q) ? 'success' : 'error';

        // }else{
        //     $data['msg'] = 'exist';
        // }
        $q = $this->changeKeys('cat_' , $r->all());
        if ($r->hasFile('banner')) {
            list($regular) = $this->uploadFiles($r, $q['cat_title'], 'banner', [$this->path]);
            $q['cat_img_path'] = $regular;
        }
        $cat = Category::create($q);
        if ($cat) {
            $this->response = ['msg' => 'success', 'd' => $this->removePrefix($cat->toArray())];
        }

        return $this->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.edit_category')->with(['cat'=>$category]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Category $category)
    {
        // return $r->all();
        unset($r->_method);
        
        $regular = '';
        $q = $this->changeKeys('cat_' , $r->all());
        if ($r->hasFile('banner')) {
            $this->removeFile($category->cat_img_path);            
            list($regular) = $this->uploadFiles($r, $q['cat_title'], 'banner', [$this->path]);
            unset($q['cat_banner']);
        }
        
        $q['cat_img_path'] = $regular;

        $w = $category->update($q);
        if ($w) {            
            $this->response['msg'] = 'successU';
        }
        return $this->response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
