<!DOCTYPE html>
<html lang="en">
<head>
	<title>Facility Management @yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<link rel="shortcut icon" href="{{ asset('images/fav.png') }}">
	<meta name="keywords" content="" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<style type="text/css">
	#loader{
		position: fixed;
		background-color: white;
		width: 100%;
		height: 1000px;
		z-index: 1111;
		text-align: center;
		padding: 20%;
	}
	#google_translate_element{
		position: absolute;
		right: 0;
		top: 0;
	}</style>

	@if (config('app.env') == 'local')
	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />	
	<link rel="stylesheet" href="{{ asset('css/chocolat.css') }}" type="text/css">	
	<link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css" media="screen" />
	<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"> 
	
	@else
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all" />	
	<link rel="stylesheet" href="{{ asset('css/chocolat.min.css') }}" type="text/css">	
	<link rel="stylesheet" href="{{ asset('css/style.min.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('css/flexslider.min.css') }}" type="text/css" media="screen" />
	<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"> 

	@endif
	
	<script type='text/javascript' src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>

	@stack('header')
</head>
<body @yield('body')>
	<div id = "loader">
		<img src="{{ asset('images/loader.gif') }}" width = "100">
	</div>
	<!-- banner -->
	<div class="banner">
		<div class="agile_dot_info">
			<div class="w3-header-bottom">
				<div class="container">
					<div class="w3layouts-logo">
						<a href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}"> </a>
					</div>


					<div class="top-nav">
						<nav class="navbar navbar-default">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<h1>Durable Facility Management Service Pvt. Ltd.</h1>
							<p> Hard work & faith is our identity</p>							
							<div class="clearfix"> </div>
						</nav>	
					</div>					
					<div class="clearfix"> </div>
					<div id="google_translate_element"></div>
				</div>
			</div>
			<div class="sub-header">
				<div class="collapse navbar-collapse  no-padding" id="bs-example-navbar-collapse-1">
					
					<ul class="nav navbar-nav">
						@forelse (App\Models\Category::get() as $cat)
						@php
						$r = route('home-search', ['name' => str_replace(' ', '-',strtolower($cat->cat_title)), 'id' => $cat->cat_id]);
						if($cat->cat_type){
							$r = route('search',['category' => $cat->subCategory->first()->scat_id]);
						}
						@endphp
						<li><a href="{{ $r }}">{{ $cat->cat_title }}</a></li>
						@empty

						@endforelse
						<li><a href="{{ route('about') }}">About Us</a></li>
						<li><a href="{{ route('payment.create') }}">Payment</a></li>
						<li><a href="{{ route('job-register') }}">Register for Job</a></li>
					</ul>	
					<div class="clearfix"> </div>
				</div>	
			</div>