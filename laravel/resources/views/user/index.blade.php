@extends('user.layouts.master')
@push('header')
{{--<link href="{{ asset('admin-assets/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">--}}
{{--<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />	--}}
@endpush
@section('content')
<div class="banner-info no">
	<div class="container">
		<div class="w3-banner-info">
			<div class="slider">
				<div class="">
					<div class="w3layouts-banner-info">
						{{-- <h4></h4>
						<h3>We provide a Variety of Services for simplifying your everyday life.</h3>
						<div class="banner_form_agileits">

							<div class=" Search-bar">
								<div class="">
									<div class="searchbar col-xs-12  col-md-5 no-padding">
										<select class="category" name="category" id = "category">
											@if (isset($catId))
											@forelse (App\Models\SubCategory::where('scat_cat_id',$catId)->get() as $c)
											<option value = "{{ $c->scat_id }}">{{ $c->scat_title }}</option>
											@empty
											@endforelse	
											@else	
											@forelse (App\Models\SubCategory::get() as $c)
											<option value = "{{ $c->scat_id }}">{{ $c->scat_title }}</option>
											@empty
											@endforelse	
											@endif
										</select>
									</div>
									<div class="searchbar col-xs-12  col-md-6 no-padding ">
										<div id="reportrange" class="search-date">
											<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
											<span></span> <b class="caret pull-right"></b>
										</div>	
										<input type="hidden" name="start" id = "from">
										<input type="hidden" name="end" id = "to">
									</div>
									<div class="searchbtn col-xs-12 col-md-1 no-padding  ">
										<button id = "search" class="btn btn-danger search-btn" type="button"><i class="fa fa-search margin-left" aria-hidden="true"></i>
										</button>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="scrollbar style-4 col-xs-12 no-padding hidden" id="searchDrop">
									<div class="force-overflow"></div>
								</div>
							</div>

						</div> --}}

					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>				
	</div>
</div>
</div>
</div>
<h3 class="heading-agileinfo">We're big on trust & safety.</h3>
<div class="border-line3"></div>
<div class="container">
	<p class="text2"> 
		We provide the Best Home Maid service, Security guard service, Housekeeping and related Maintenance Services across the All India. We provide services  for corporate office, Office campus, Industries, IT Industries, Institutes and Commercial Complex, Homes and Individuals.
	</p>
	<p class="text2">
		We assure you, “Trust and Safety”. We supply you, personnel on time in demanded trait. Only hardworking, honest, qualified and verified workers are supplied by our company.
	</p>
	<p class="text2">
		We have conglomerate platform for the personnel who serve you in the best way in terms of their superior skills and master in the specialized field. We provide various Home maid, housekeeping, maintenance, Security and Engineering services.
	</p>
</div>
<div class="about-grid">

	<div class="col-md-6 about-sub-grid">
		<div class="col-md-6 about-right about-right-flex ">
			<div class="about-bottom-text">
				<h4 class="title1">EASY & QUICK SERVICE </h4>
				<p class="text">Our Online platform allows you to search for services you are looking for. You can enquire for the corresponding available personnel that you wish to hire. 
				</p>
				<p class="text">
					We immediate revert you back to take care of your enquiry. Client is our Focus.We persuade to fulfill our client’s requirements.

				</p>
			</div>
			<div class="about-bottom-grid about-img1">
			</div>
		</div>
		<div class="col-md-6 about-right about-right-flex">
			<div class="about-bottom-grid about-img2">
			</div>
			<div class="about-bottom-text ab1">
				<h4 class="title1">QUALITY ASSURANCE </h4>
				<p class="text">“ No one is one’s master ” . Personnel we provide are superior, skilled, zealous and diligent that assure you quality and on time service in reasonable and reliable charges.
				</p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>	
	<div class="col-md-6 about-bottom-grid about-bg"></div>
	<div class="clearfix"></div>
</div>
<!-- //about -->
<!-- news -->
<div class="news">
	<div class="container">  
		<h2 class="heading-agileinfo">Why to choose Us?</h2>
		<div class="border-line3"></div>
		<div class="news-agileinfo"> 
			<div class="news-w3row"> 
				<div class="col-md-6 wthree-news-grids bor-b bor-r">
					<div class="col-md-3 col-xs-3 datew3-agileits">
						<img src="images/t1.jpg" class="img-responsive" alt=""/>
					</div>
					<div class="col-md-9 col-xs-9 datew3-agileits-info ">
						<h5><a href="#" data-toggle="modal" data-target="#myModal">Reliable</a></h5>

						<p>Our services have set the benchmark in the facility management services.
						We are giving consistent and trustworthy performance to our clients.We are flexible to work as per your needs & convenience.</p>

					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 wthree-news-grids news-grids-mdl bor-b ">
					<div class="col-md-3 col-xs-3 datew3-agileits datew3-agileits-fltrt">
						<img src="images/t2.jpg" class="img-responsive" alt=""/>
					</div>
					<div class="col-md-9 col-xs-9 datew3-agileits-info datew3-agileits-info-fltlft">
						<h5><a href="#" data-toggle="modal" data-target="#myModal">Affordable & Quick</a></h5>

						<p>We provide services at an affordable cost. Services we offer are more than worth to pay and are very quick and on timely basis.                                     </p>
						<p>   </p>
						<p> </p>
					</div>
					<div class="clearfix"> </div>
				</div> 
				<div class="col-md-6 wthree-news-grids bor-b bor-r">
					<div class="col-md-3 col-xs-3 datew3-agileits">
						<img src="images/c4.jpg" class="img-responsive" alt=""/>
					</div>
					<div class="col-md-9 col-xs-9 datew3-agileits-info ">
						<h5><a href="#" data-toggle="modal" data-target="#myModal">Verified Service</a></h5>

						<p>Personnel we supply are verified by our key team before we make them avail  you. Workers are closely evaluated for their skills, qualities and health.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 wthree-news-grids news-grids-mdl bor-b ">
					<div class="col-md-3 col-xs-3 datew3-agileits datew3-agileits-fltrt">
						<img src="images/c5.jpg" class="img-responsive" alt=""/>
					</div>
					<div class="col-md-9 col-xs-9 datew3-agileits-info datew3-agileits-info-fltlft">
						<h5><a href="#" data-toggle="modal" data-target="#myModal">Quality Service</a></h5>

						<p>Verified personnel and our diligent staff assures you quality of services in terms of availability, reliability, Timeliness, skilled and honest workers & Cost effective solution.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>

		</div>
	</div>
</div>
<!-- //news -->

<!-- /services -->
<div class="w3-services">
	<div class="container">
		<h3 class="heading-agileinfo" style= "color: #fff;padding-bottom: 10px;border-bottom: 1px #cccccc78 solid;">
			Services we offer?
		</h3>
		<div class="w3-services-right-grid">
			<div class="w3-icon-grid-gap1">
				<div class="col-md-4 w3-icon-grid1 bor-r bor-b">
					<i class="fa fa-calendar-check-o" aria-hidden="true"></i>
					<h3>Home Maid Service</h3>
					<p>Our Home maid Services are diversified in - </p>
					<p>1. Wash clothes and utensils</p>
					<p>2. Cooking</p>
					<p>3. Bathing and massage the baby</p>
					<p>4. Patients care</p>
					<p>5. Dusting</p>
					<p>6. senior citizen care</p>
					<p>  </p>
				</p>
			</div>

			<div class="col-md-4 w3-icon-grid1 bor-r bor-b">
				<i class="fa fa-calendar-check-o" aria-hidden="true"></i>
				<h3>Security Guard Service </h3>
				<p>We Provide trained security guards and officers at verious places at india such as -   </p>
				
				<div class="col-md-6 no-padding"><p>1.Big Housing Security</p></div>
				<div class="col-md-6 no-padding"><p>2.Banks</p></div>				
				<div class="col-md-6 no-padding"><p>3.Hostels</p></div>				
				<div class="col-md-6 no-padding"><p>4.Hospital</p></div>				
				<div class="col-md-6 no-padding"><p>5.Hospitals</p></div>				
				<div class="col-md-6 no-padding"><p>6.Colleges</p></div>				
				<div class="col-md-6 no-padding"><p>7.Corporate Offices &amp; Companies</p></div>
				<div class="col-md-6 no-padding"><p>8.And in evry quarter of India where there is a need for trained security guards.</p></div>
			</div>

			<div class="col-md-4 w3-icon-grid1 bor-b">
				<i class="fa fa-calendar-check-o" aria-hidden="true"></i>
				<h3>Other services</h3>
				<p>Other Services includes following services - </p>
				<p>1. Nursing              2. Painting</p>
				<p>3. Office boy           4. Labour service</p>
				<p>5. Maintenance          6. Cleaning</p>
				<p>7. Hotel Service         8. Gardening  </p>
				<p>9. Pest-control        10. Shifting  </p>
				<p>11. Farming               12.Driving </p>
				<p>   </p>				
			</div>
			<div class="clearfix"></div>
		</div>

	</div>
</div>
<div class="clearfix"></div>
</div>
</div>

<!-- //services -->
<!-- testimonials -->
<div class="testimonials">
	<div class="container">
		<h3 class="heading-agileinfo">Testimonials<span>Our Client Says!</span></h3>
		<div class="flexslider-info">
			<section class="slider1">
				<div class="flexslider">
					<ul class="slides">
						<li>
							<div class="w3l-info1">
								<div class="col-md-3 testimonials-grid-1">

								</div>
								<div class="col-md-9 testimonials-grid-2">
									<h4>Mr. Richard</h4>
									<h5>Senior Architect</h5>
									<p>I have been searching for skilled and quality home maid for so long.Durability facility management services provided a candid personel for me. I am very happy with the services they have given.Thank you DFMS! </p>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l-info1">
								<div class="col-md-3 testimonials-grid-1">

								</div>
								<div class="col-md-9 testimonials-grid-2">
									<h4>Ms. Neha</h4>
									<h5>Software Professional</h5>
									<p>Excellent service by DFMS. It's staff is very polity, respnsive and helpful.They made available Office boy for our offices. The Staff they provide is very genuine, skilfull and hardworking.</p>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l-info1">
								<div class="col-md-3 testimonials-grid-1">

								</div>
								<div class="col-md-9 testimonials-grid-2">
									<h4>Mrs. Riya</h4>
									<h5>Home Maker</h5>
									<p>I am taking quality staff from DFMS for a long period.I used to get services for different area as plumbing, painting home, home maid, Pest-control, Carpenter. They provide skilled personel.Thank you DFMS...</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</section>
		</div>
	</div>
</div>
<!-- //testimonials -->

@endsection

@push('footer')
{{--<script src="{{ asset('admin-assets/moment/min/moment.min.js') }}"></script>--}}
{{--<script src="{{ asset('admin-assets/bootstrap-daterangepicker/daterangepicker.js') }}"></script>--}}
{{--<script src="{{ asset('js/select2.min.js') }}"></script>--}}

<script type="text/javascript">
	{{--$('.category').select2({--}}
	{{--placeholder: 'Start typing slowly'--}}
	{{--});--}}
	{{--function init_daterangepicker() {--}}
	{{--if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }--}}
	{{--var cb = function(start, end, label) {--}}
	{{--$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));--}}
	{{--};--}}

	{{--var optionSet1 = {--}}
	{{--startDate: moment(),--}}
	{{--endDate: moment().add(29, 'days'),--}}
	{{--minDate: '01/01/2018',--}}
	{{--maxDate: '12/31/2019',--}}
	{{--dateLimit: {--}}
	{{--days: 60--}}
	{{--},--}}
	{{--showDropdowns: true,--}}
	{{--showWeekNumbers: true,--}}
	{{--timePicker: false,--}}
	{{--timePickerIncrement: 1,--}}
	{{--timePicker12Hour: true,--}}
	{{--ranges: {--}}
	{{--'Today': [moment(), moment()],--}}
	{{--'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],--}}
	{{--'Last 7 Days': [moment().subtract(6, 'days'), moment()],--}}
	{{--'Last 30 Days': [moment().subtract(29, 'days'), moment()],--}}
	{{--'This Month': [moment().startOf('month'), moment().endOf('month')],--}}
	{{--'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]--}}
	{{--},--}}
	{{--opens: 'left',--}}
	{{--buttonClasses: ['btn btn-default'],--}}
	{{--applyClass: 'btn-small btn-primary',--}}
	{{--cancelClass: 'btn-small',--}}
	{{--format: 'MM/DD/YYYY',--}}
	{{--separator: ' to ',--}}
	{{--locale: {--}}
	{{--applyLabel: 'Submit',--}}
	{{--cancelLabel: 'Clear',--}}
	{{--fromLabel: 'From',--}}
	{{--toLabel: 'To',--}}
	{{--customRangeLabel: 'Custom',--}}
	{{--daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],--}}
	{{--monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],--}}
	{{--firstDay: 1--}}
	{{--}--}}
	{{--};--}}
	{{--$('#from').val(moment().format('YYYY-MM-DD'));--}}
	{{--$('#to').val(moment().add(29, 'days').format('YYYY-MM-DD'));--}}
	{{--$('#reportrange span').html(moment().format('MMMM D, YYYY') + ' <b>to</b> ' + moment().add(29, 'days').format('MMMM D, YYYY'));--}}
	{{--$('#reportrange').daterangepicker(optionSet1, cb);--}}
	{{--$('#reportrange').on('apply.daterangepicker', function(ev, picker) {		--}}
	{{--$('#from').val(picker.startDate.format('YYYY-MM-DD'));--}}
	{{--$('#to').val(picker.endDate.format('YYYY-MM-DD'));--}}
	{{--});--}}


	{{--}--}}
	{{--init_daterangepicker();--}}

	{{--$('#search').on({--}}
	{{--'click' : function(e){--}}
	{{--e.preventDefault();--}}
	{{--location.href = '{{ url('search') }}/'+$('#category').val()+'/'+$('#from').val()+'/'+$('#to').val();			--}}
	{{--}--}}
	{{--});--}}
</script>
@endpush