@extends('user.layouts.master')

@push('header')
@php
$ID = 'worker';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
<style type="text/css">

img {
	object-fit: contain;
}
</style>
<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />

@endpush

@section('content')
<section class="fac-list">
	<div class="container fac-list1" id = "regForm">
		<h2 class="text-center">Register here for Job</h2>		
		<form id = "{{ $ID }}Form">
			<div class="w3layouts_mail_grid_right mail_grid no-padding">						
				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label">Employee Image</label>
						<div class="text-center">
							<img src="{{ asset('images/blank-user.jpg') }}" width="150" height="150" id = "user-imgPreview">
							<div class="clearfix"></div>
							<input type = "file" id ="user-img" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="user_img">
							<label class="btn btn-success" for = "user-img">Choose User Image</label>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group text-center">
						<label class="control-label">Employee ID:</label>
						@php
						$empId = 'DFMS'.substr((\App\User::max('id') + 10000001),1,8);
						@endphp
						<label class="control-label">{{ $empId }}</label>
					</div>
				</div>
				<div class="col-md-5">
					<input type="hidden" name="emp_id" value="{{ $empId }}">
					<div>
						<label class="control-label">Full Name</label>
						<input type="text" class="form-control col-md-7 col-xs-12" name = "name" data-validate = "empty|alphaSpace" autocomplete="off">
						<div class="clearfix"></div>
					</div>
					<div>
						<label class="control-label">Mobile</label>
						<input type="number" class="form-control col-md-7 col-xs-12" name = "mobile" data-validate = "empty|mobile" autocomplete="off">
						<div class="clearfix"></div>

					</div>
					<div>
						<label class="control-label">Alternate Mobile</label>
						<input type="number" class="form-control col-md-7 col-xs-12" name = "alt_mobile" autocomplete="off">
						<div class="clearfix"></div>

					</div>
					<div>
						<label class="control-label">Email</label>
						<input type="email" class="form-control col-md-7 col-xs-12" name = "email" data-validate = "empty|email" autocomplete="off">
						<div class="clearfix"></div>

					</div>

					<div>
						<label class="control-label">Date of Birth</label>					
						<input type="date" class="form-control col-md-7 col-xs-12" name = "dob" autocomplete="off">
						<div class="clearfix"></div>
					</div>
					<div style="margin-bottom: 35px;">
						<label class="control-label">Gender</label>
						<div class="clearfix"></div>
						<div>							
							<input type="radio" name="gender" value = "0" checked> Male
							<input type="radio" name="gender" value = "1"> Female
						</div>
						<div class="clearfix"></div>
					</div>					
					<div>
						<label class="control-label">Parmanent Address</label>
						<textarea class="form-control" name="parmanet_address"></textarea>
						<div class="clearfix"></div>
					</div>	
				</div>
				<div class="col-md-5 ">					
					<div class="form-group">
						<label class="control-label">Category for Work</label>

						<select class="form-control col-md-7 col-xs-12 cat" name="cat[]" multiple="multiple">
							<option value = "-1">--select--</option>
							@forelse (App\Models\Category::get() as $cat)
							<optgroup label="{{ $cat->cat_title }}"></optgroup>
							@forelse (App\Models\SubCategory::where('scat_cat_id', $cat->cat_id)->get() as $c)										
							<option value = "{{ $c->scat_id }}">{{ $c->scat_title }}</option>
							@empty										
							@endforelse
							@empty
							@endforelse									
						</select>									
						<div class="clearfix"></div>

					</div>
					<div class="form-group">
						<label class="control-label">Known Languages</label>

						<select class="form-control col-md-7 col-xs-12 lang" name="lang[]" multiple="multiple">
							<option value = "-1">--select--</option>
							@forelse (App\Models\Language::get() as $l)
							<option value = "{{ $l->lan_id }}">{{ $l->lan_title }}</option>
							@empty
							@endforelse									
						</select>
						<div class="clearfix"></div>

					</div>
					<div class="form-group">
						<label class="control-label">Qualifications</label>

						<select class="form-control col-md-7 col-xs-12 qua" name="qualifications[]" multiple="multiple">
							<option value = "-1">--select--</option>
							@forelse (App\Models\Qualification::get() as $q)
							<option value = "{{ $q->qua_id }}">{{ $q->qua_title }}</option>
							@empty
							@endforelse									
						</select>									
						<div class="clearfix"></div>

					</div>
					<div class="form-group">
						<label class="control-label">Mother Language</label>

						<select class="form-control col-md-7 col-xs-12 tongue" name="mother_tongue">
							<option value = "-1">--select--</option>
							@forelse (App\Models\Language::get() as $la)
							<option value = "{{ $la->lan_id }}">{{ $la->lan_title }}</option>
							@empty
							@endforelse
						</select>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<label class="control-label">Blood Group</label>

						<select class="form-control col-md-7 col-xs-12 blood-group" name="blood_group">
							<option value = "-1">--select--</option>
							@forelse (App\Models\BloodGroup::get() as $bg)
							<option value = "{{ $bg->bg_id }}">{{ $bg->bg_title }}</option>
							@empty
							@endforelse
						</select>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<label class="control-label">Height</label>
						<div class="clearfix"></div>						
						<div class="col-md-3 no-padding">							
							<input type="number" name="height_feet" placeholder="Feet" autocomplete="off">
						</div>
						<div class="col-md-3 no-padding">
							<input type="number" name="height_inches" placeholder="Inches" autocomplete="off">
						</div>
						<div class="clearfix"></div>
					</div>
					<div>
						<label class="control-label">Current Address</label>
						<textarea class="form-control" name="current_address"></textarea>
						<div class="clearfix"></div>
					</div>	
				</div>
			</div>
			<div class="col-md-12">
				<h3>Upload Documents</h3>
				<div class="form-group col-md-6 no-padding">
					<label class="control-label">Identity Proof
					</label>							
					<div class="text-center">
						<input type="text" class="form-control col-md-7 col-xs-12" name = "identity_no" autocomplete="off">
						<div class="clearfix"></div>
						<img class = "id-card" src="{{ asset('images/no-image.png') }}" id = "id-cardPreview">
						<div class="clearfix"></div>
						<input type = "file" id ="id-card" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="id_card">
						<label class="btn btn-success" for = "id-card">Choose Pan Card or Aadhar Card</label>
					</div>
				</div>
				<div class="form-group col-md-6 no-padding">
					<label class="control-label">Address Proof
					</label>							
					<div class="text-center">
						<input type="text" class="form-control col-md-7 col-xs-12" name = "address_no" autocomplete="off">
						<img class = "id-card" src="{{ asset('images/no-image.png') }}" id = "add-cardPreview">
						<div class="clearfix"></div>
						<input type = "file" id ="add-card" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="add_card">
						<label class="btn btn-success" for = "add-card">Choose Electriciy Bill or Election Card</label>
					</div>
				</div>	
			</div>
			<div class="clearfix"></div>
			<div class="text-center">
				<button type="submit" class="btn btn-default bton">Register</button>
			</div>
		</form>
	</div>
	<div class = "hidden" id = "success">
		<h1 class="text-center" style = "padding: 10%;">Welcome to Durable Facility Management Service Pvt. Ltd. Please note your Employee ID - <b>{{ 'DFMS'.substr((\App\User::max('id') + 10000001),1,8) }}</b></h1>
	</div>
</section>
@endsection

@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route($ID.'.store') }}',		
		processResponse : function (data) {
			if(data.msg == "success"){			
				var blank = '{{ asset("images/no-image.png") }}',
				userBlank = '{{ asset('images/blank-user.jpg') }}';
				$('.qua, .lang, .cat').val(null).trigger('change');			
				$('#user-imgPreview').attr('src' , userBlank);
				$('#id-cardPreview, #add-cardPreview').attr('src', blank);
				$('#regForm').addClass('hidden');
				$('#success').removeClass('hidden');
				$('html, body').animate({scrollTop : 0},600);
			}
		}
	});	
	function geturl(name) {
		var link = '{{ route('home') }}/'+name;
		return link;
	}

	multiSelect('.qua', 'qualification');
	multiSelect('.lang', 'language');
	multiSelect('.cat', 'sub-category');
	multiSelect('.tongue', 'language');
	multiSelect('.blood-group', 'blood-group');

	imageUpload('id-card');
	imageUpload('add-card');
	imageUpload('user-img');
</script>
@endpush
