@extends('user.layouts.master')

@push('header')
@isset ($MERCHANT_KEY)

<script>
	var hash = '{{ $hash }}';	
	function submitPayuForm() {
		if(hash == '') {
			return
		}else{
			$('#loader').show();		
		}
		var payuForm = document.forms.payuForm
		payuForm.submit()
	}
</script>
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endisset
@endpush

@section('body')
onload="submitPayuForm();"
@endsection

@section('content')
<section class="fac-list">
	<div class="container fac-list1">		
		<div class="col-md-6 col-md-offset-3">			
			<h2 class="text-center">Payment</h2>
			<form action="{{ $action }}" method="post" name="payuForm">		
				{{ csrf_field() }}
				<input type="hidden" name="key" value="{{ $MERCHANT_KEY }}" />
				<input type="hidden" name="hash" value="{{ $hash }}"/>
				<input type="hidden" name="txnid" value="{{ $txnid }}" />
				<input type="hidden" name="service_provider" value="payu_paisa" size="64" />
				<div class="w3layouts_mail_grid_right mail_grid">

					<input type = "hidden" name="productinfo" value="{{  (empty($posted['productinfo'])) ? 'Payment from Durable Facility Management Services' : $posted['productinfo'] }}" size="64" />


					<input type = "hidden" name="surl" value="{{ (empty($posted['surl'])) ? route('orderStatus') : $posted['surl'] }}" size="64" />

					<input type = "hidden" name="furl" value="{{  (empty($posted['furl'])) ? route('orderStatus') : $posted['furl']  }}" size="64" />


					<div>
						<label class="control-label">Amount</label>
						<input required class="form-control" min="0" name="amount" type="number"  value="{{ (empty($posted['amount'])) ? '' : $posted['amount'] }}" data-validate = "empty|digits" />					
					</div>
					<div style="margin-bottom: 10px;">
						<label class="control-label">Company(Optional)</label>
						<select name="company" class="company">
							<option value="-1">--Select--</option>
							@forelse (\App\Models\Company::get() as $c)
							<option value="{{ $c->cmp_id }}" {{ (empty($posted['company'])) ? '' : (($posted['company'] == $c->cmp_id ) ? 'selected' : '') }}>{{ $c->cmp_title }}</option>
							@empty								
							@endforelse
						</select>						
					</div>
					<div>
						<label class="control-label">Full Name</label>
						<input required class="form-control" name="firstname" type="text" id="firstname" value="{{ (empty($posted['firstname'])) ? '' : $posted['firstname'] }}"  data-validate = "empty|alphaSpace"/>					
					</div>
					<div>
						<label class="control-label">Email:</label>
						<input required class="form-control" name="email" type="email" autocomplete="off" id="email" value="{{ (empty($posted['email'])) ? '' : $posted['email'] }}"  data-validate = "empty|email"/>
					</div>
					<div>
						<label class="control-label">Mobile</label>
						<input required id = "phone" class="form-control" type="number" name="phone" value="{{ (empty($posted['phone'])) ? '' : $posted['phone'] }}"  data-validate = "empty|mobile"/>
					</div>
					{{-- <div>
						<label class="control-label">City</label>
						<input class="form-control" type="text"  name="city" value = "{{ (empty($posted['city'])) ? '' : $posted['city'] }}">
					</div>				
					<div>
						<label class="control-label">Pincode</label>					
						<input class="form-control" type="number" name="pincode" value = "{{ (empty($posted['pincode'])) ? '' : $posted['pincode'] }}">

						<div class="clearfix"></div>
					</div>
					<div>
						<label class="control-label">Address</label>
						<textarea class="form-control" name="address" required>{{ (empty($posted['address'])) ? '' : $posted['address'] }}</textarea>												
					</div> --}}



					<input type="hidden" name="udf1" value="{{ (empty($posted['udf1'])) ? '' : $posted['udf1'] }}" />

					<input type="hidden" name="udf2" value="{{ (empty($posted['udf2'])) ? '' : $posted['udf2'] }}" />

					<input type="hidden" name="udf3" value="{{ (empty($posted['udf3'])) ? '' : $posted['udf3'] }}" />

					<input type="hidden" name="udf4" value="{{  (empty($posted['udf4'])) ? '' : $posted['udf4'] }}" />

					<input type="hidden" name="udf5" value="{{ (empty($posted['udf5'])) ? '' : $posted['udf5'] }}" />

					<input type="hidden" name="pg" value="{{(empty($posted['pg'])) ? '' : $posted['pg'] }}" type="hidden" />
					<div class="text-center">
						<button id = "proceedBtn" type="submit" class="btn btn-default bton">Pay Now via PayUMoney</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</section>
@endsection

@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
	$('.company').select2();
</script>
@endpush
