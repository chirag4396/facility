@extends('user.layouts.master')

@push('header')
@php
$ID = 'enquiry';
@endphp
<script type="text/javascript">
	var ID = '{{ $ID }}';
</script>
<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />
@php
$cats = \App\Models\SubCategory::whereIn('scat_id',explode(',',$category))->get();
$t = [];
$mc = [];
foreach ($cats as $k => $v) {		
	$t[] = $v->scat_title;
	$mc[] = $v->scat_cat_id;
}
$title = implode(', ',$t);

@endphp
@endpush

@section('content')
@if ($category == '35')

<div class="container">
	<h2 class="heading-agileinfo sub-head security-h">					
		<span class="security-h-span">We supply trained security guards and officers at various places in India such as Big Housing Societies, Various Companies, Banks, Corporate offices, Hotels, Colleges, Hostels, Hospitals and in every quarter of India where there is a need for trained Security guards.</span>
	</h2>		
</div>
@endif

<section class="fac-list">
	<div class="container fac-list1">
		<div class="col-md-8 col-xs-12 ">
			<div class = "sub-banner" style="background: url('{{ asset($cats->first()->scat_img_path) }}');"></div>
			<div class="col-xs-12 f-title">
				<div class="col-md-6"><h4>{{ $title  }}</h4></div>
				<div class="col-md-2"><button type="button" onclick="hire(0);">Enquiry</button></div>
				<div class="col-md-4 st"><strong>{{ number_format($users->count()*123) }}: People </strong> Available <img src="{{ asset('images/qq.png') }}"></div>
			</div>
			<div class="clearfix"></div>
			<div>
				<div class="alert text-center not-hire">
					<p>You are not Authorized to view Employees, please contact us for Hirings.</p>
					<button type="button" onclick="hire(0);">Enquiry</button>
				</div>
			</div>

			{{-- Logic to show users --}}
			<div class = "hidden">
			{{-- <table class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Gender</th>						
						<th>Particular Hiring</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($users as $k => $user)						
					<tr>
						<td>{{ ++$k }}</td>
						<td>{{ $user->user->name }}</td>
						<td>{{ $user->user->gender ? 'Female' : 'Male' }}</td>						
						<td><button type="button" onclick="hire({{ $user->ud_user }}, '{{ $user->user->name }}');">Hire Me</button></td>
					</tr>
					@empty
					<tr>
						<td colspan="4"> 
							<h3 class="text-center">No job seeker is available for {{ $cat->scat_title }}, still you can enquiry for them.</h3>
							<button type="button" onclick="hire(0);">Enquiry for {{ $cat->scat_title }}</button>
						</td>
					</tr>
					@endforelse				
				</tbody>
			</table> --}}
		</div>			
	</div>
	<div id="{{ $ID }}Modal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body w3layouts_mail_grid_right mail_grid">
					<div id = "mobile-box">
						Enter Mobile Number:
						<input type="text" name="mobile" placeholder="Mobile" required id ="mobile" autocomplete="off" autosave="off">
						<div id = "otp-box" class="hidden">
							<div class="col-md-4 no-padding">
								Enter OTP here : <input type="text" id ="otp"  autocomplete="off" autosave="off">	  
							</div>							
							<div class="col-md-2 no-padding resend">
								<button id = "resend" type="button" class="btn btn-default bton" disabled>Resend<i id="timer"></i></button>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="alert alert-warning text-center hidden" id = "mobile-msg"></div>
						<div class="clearfix"></div>
					</div>
					<div class="hidden" id = "enq-box">
						<form id = "{{ $ID }}Form">
							<input type="hidden" name="user" id = "user" value = "0">
							<input type="hidden" name="category" value = "{{ $category }}" id = "category">
							<div>
								<label>Full Name:</label>
								<input type="text" name="full_name" required>
							</div>
							<div>
								<label>Company Name:</label>
								<input type="text" name="title">
							</div>
							<div>
								<label>Mobile:</label>
								<input type="number" name="mobile" id ="enq-mobile">
							</div>
							<div>
								<label>Email:</label>
								<input type="email" name="email" placeholder="Email" required>
							</div>
							<div>
								<label>City:</label>
								<input type="text" name="city" id ="enq-mobile">
							</div>
							<div class="col-md-6 no-padding">
								<label>From:</label>
								<input type="date" name="from" value="{{ $from }}">
							</div>
							<div class="col-md-6 no-padding">
								<label>To:</label>
								<input type="date" name="to" value="{{ $to }}">
							</div>
							<div>								
								<label>Query</label>
								<textarea name = "query" onfocus="$(this).val('');" onblur="$(this).val('Query');">Query</textarea>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-default">Enquire Now</button>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="col-md-4 col-xs-12 ff">
		<div style="background: #fff;" class="col-xs-12  f-title">
			<div class="col-md-12 "> <h4 >Related search results</h4></div>
		</div>
		@forelse (\App\Models\SubCategory::whereNotIn('scat_id',explode(',',$category))->whereIn('scat_cat_id', $mc)->limit(5)->get() as $scat)
		<a class = "related" href="{{ route('search', ['category' => $scat->scat_id]) }}">
			<div class="col-md-12 f-right ">
				<img src="{{ asset($scat->scat_img_thumb_path) }}" class="img-responsive" alt="{{ $scat->scat_title }}">
				<h4>{{ $scat->scat_title }}</h4>
			</div>
		</a>				
		@empty

		@endforelse
			{{-- <div class="col-md-12 f-right ">
				<img src="{{ asset('images/e2.jpg') }}" class="img-responsive" alt="img">
				<h4>Home Nurse</h4>
			</div>
			<div class="col-md-12 f-right ">
				<img src="{{ asset('images/e3.jpg') }}" class="img-responsive" alt="img">
				<h4>Plumber</h4>
			</div> --}}
		</div>
	</div>
</section>
@endsection

@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>

<script type="text/javascript">
	var userI = 0;
	var catI = "{{ $category }}";
	function getTimeRemaining(endtime) {
		var t = Date.parse(endtime) - Date.parse(new Date());
		var seconds = Math.floor((t / 1000) % 60);
		var minutes = Math.floor((t / 1000 / 60) % 60);
		var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
		var days = Math.floor(t / (1000 * 60 * 60 * 24));
		return {
			'total': t,
			'days': days,
			'hours': hours,
			'minutes': minutes,
			'seconds': seconds
		};
	}

	function initializeClock(id, endtime) {
		// var clock = document.getElementById(id);
		// var daysSpan = clock.querySelector('.days');
		// var hoursSpan = clock.querySelector('.hours');
		// var minutesSpan = clock.querySelector('.minutes');
		// var secondsSpan = clock.querySelector('.seconds');

		function updateClock() {
			var t = getTimeRemaining(endtime);

			// daysSpan.innerHTML = t.days;
			// hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
			// minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
			// secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
			$('#'+id).html(('0' + t.minutes).slice(-2) +':'+('0' + t.seconds).slice(-2));
			if (t.total <= 0) {
				clearInterval(timeinterval);
			}
		}

		updateClock();
		var timeinterval = setInterval(updateClock, 1000);
	}

	var deadline = new Date(Date.parse(new Date()) + 1 * 1 * 2 * 60 * 1000);
	

	hire = function(id, name = null){
		$('#mobile-box').removeClass('hidden');		
		$('#otp-box').addClass('hidden');		
		$('#enq-box').addClass('hidden');		
		// $('#'+ID+'Form')[0].reset();
		$('#'+ID+'Modal').modal('toggle');		
		$('.modal-title').html((id) ? 'Hire '+name : 'Bulk Hiring');		
		$('#user').val(id);
	}
	// hire(0);
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route($ID.'.store') }}',
		validation:false,	
		processResponse : function (data) {
			if(data.msg == "success"){
				$('#formAlert').html('We will contact you soon!!');
			}
		}
	});		
	function generate(val) {
		if(val.length > 9){
			$('#mobile-msg').removeClass('hidden').html('Sending OTP, please wait.');
			$('#otp').removeClass('hidden');
			$.post('{{ route('otp') }}', {mobile: val}, function(data) {
				if(data.msg == 'success'){
					$('#otp-box').removeClass('hidden');
					$('#otp').focus();
					$('#mobile').attr({'readonly' : true, 'disabled' :true});
					initializeClock('timer', deadline);
					window.setTimeout(function () {
						$('#timer').parent().attr('disabled', false);
					},120000);
					$('#mobile-msg').addClass('hidden');
				}
			});
		}
	}
	$('#mobile').on({
		'keyup' : function(){
			var val = this.value;
			generate(val);
		}
	});
	$('#otp').on({
		'keyup' : function(){
			var otp = this.value,
			mobile = $('#mobile').val();
			
			if(otp.length > 5){	
				$('#mobile-msg').removeClass('hidden').html('Checking OTP, please wait.');
				$('#otp').attr({'readonly' : true, 'disabled' :true});					
				$.post('{{ route('check-otp') }}', {mobile: mobile, otp: otp}, function(data) {
					if(data.msg == 'success'){
						$('#enq-mobile').val(mobile);
						$('#mobile-box').addClass('hidden');
						$('#enq-box').removeClass('hidden');
						$('#mobile-msg').addClass('hidden');
						if(data.user != 'new'){						
							$.each($('#'+ID+'Form')[0],function(k, v) {
								var name = $(v).attr('name');
								console.log(name);
								if(name == 'user'){
									$(v).val(userI);
								}else if(name == 'category'){
									$(v).val(catI);
								}else{									
									$(v).val(data.user[name]);
								}
							});
						}
					}
					if(data.msg == 'error'){
						$('#otp').attr({'readonly' : false, 'disabled' :false}).focus();
						$('#mobile-msg').removeClass('hidden').html('Incorrect OTP, Please try again');
						window.setTimeout(function () {
							$('#mobile-msg').addClass('hidden').html('');							
						},2000);
					}
				});
			}
		}
	});

	$('#resend').on({
		'click' : function(){			
			var val = $('#mobile').val();			
			generate(val);
		}
	});
</script>
@endpush
