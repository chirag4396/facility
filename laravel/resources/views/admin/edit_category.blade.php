@extends('admin.layouts.master')
@section('title')
Create New Category
@endsection
@php
$ID = 'category';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
<style type="text/css">
img {
	object-fit: contain;
}
</style>
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Create New Category</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<br />
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">
						<input type="hidden" name="id" value="{{ $cat->cat_id }}">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Category Title
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" class="form-control col-md-7 col-xs-12" id = "typeName" name = "title" value="{{ $cat->cat_title }}">
							</div>
						</div>						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Category Banner
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<img src="{{ asset(empty($cat->cat_img_path) ? 'images/no-image.png' : $cat->cat_img_path) }}" width="300" height="150" id = "banPreview">
								<div class="clearfix"></div>
								<input type = "file" id ="ban" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="banner">
								<label class="btn btn-success" for = "ban">Choose Image</label>
							</div>
						</div>						
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>							
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script>
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.index') }}',
		validation:false,	
		type : 2,
		processResponse : function (data) {
			console.log(data);
		}
	});	
	imageUpload('ban');	
</script>
@endpush